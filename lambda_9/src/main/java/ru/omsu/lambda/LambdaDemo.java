package ru.omsu.lambda;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaDemo {
    public static final Function<String, Integer> getStringLength = String::length;
    public static final Function<String, Character> getStringFirstChar = str -> str.isEmpty() ? null : str.charAt(0);
    public static final Predicate<String> isStringNotContainsSpaces = str -> !str.contains(" ");
    public static final Function<String, Integer> getStringWordsAmount = str -> str.isEmpty() ? 0 : str.split(",").length;
    public static final Function<Human, Integer> getHumanAge = Human::getAge;
    public static final BiPredicate<Human, Human> isHumanLastNamesEquals =
            (human1, human2) -> human1.getLastName().equals(human2.getLastName());

    public static final Function<Human, String> getHumanFullName =
            human -> human.getLastName() + " " + human.getFirstName() + " " + human.getPatronymic();

    public static final Function<Human, Human> setHumanOlder =
            human -> new Human(human.getFirstName(), human.getLastName(), human.getPatronymic(), human.getAge() + 1, human.getSex());

    public static final QuadruplePredicate<Human, Human, Human, Integer> checkHumanMaxAge =
            (h1, h2, h3, age) -> h1.getAge() < age && h2.getAge() < age && h3.getAge() < age;

    @FunctionalInterface
    public interface QuadruplePredicate<T, M, N, L> {
        boolean test(T first, M second, N third, L fourth);
    }

}
