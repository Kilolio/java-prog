package ru.omsu.lambda;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamApiDemo extends LambdaDemo {
    public static final Function<List<Object>, List<Object>> deleteNullObjects =
            objects -> objects
                    .stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

    public static final Function<Set<Integer>, Long> countPositiveNumbers =
            numbers -> numbers
                    .stream()
                    .filter(number -> number > 0)
                    .count();

    public static final Function<List<Object>, List<Object>> getThreeLastObjects =
            objects -> objects
                    .stream()
                    .skip(objects.size() - 3)
                    .collect(Collectors.toList());

    public static final Function<List<Integer>, Integer> getFirstEvenNumber =
            numbers -> numbers
                    .stream()
                    .filter(number -> number % 2 == 0)
                    .findFirst()
                    .orElse(null);

    public static final Function<int[], List<Integer>> getSquaredNumbers =
            numbers -> Arrays.stream(numbers)
                    .distinct()
                    .map(number -> number * number)
                    .boxed()
                    .collect(Collectors.toList());

    public static final Function<List<String>, List<String>> getNotEmptyStrings =
            strings -> strings
                    .stream()
                    .filter(string -> !string.isEmpty())
                    .sorted()
                    .collect(Collectors.toList());

    public static final Function<Set<String>, List<String>> getListOfStringDest =
            strings -> strings
                    .stream()
                    .sorted(Comparator.reverseOrder())
                    .collect(Collectors.toList());

    public static final Function<Set<Integer>, Integer> getSumOfSquaredNumbers =
            numbers -> numbers
                    .stream()
                    .reduce(0, (number1, number2) -> number1 + number2 * number2);

    public static final Function<Collection<Human>, Integer> getMaxHumanAge =
            humans -> humans
                    .stream()
                    .map(Human::getAge)
                    .max(Integer::compareTo)
                    .orElse(0);

    public static final Function<Collection<Human>, Collection<Human>> getSortedHumans =
            humans -> humans
                    .stream()
                    .sorted(Comparator.comparing(Human::getSex).thenComparingInt(Human::getAge))
                    .collect(Collectors.toList());

}
