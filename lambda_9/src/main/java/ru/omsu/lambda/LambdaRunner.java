package ru.omsu.lambda;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaRunner {
    public static <K, V> V functionRunner(Function<K, V> function, K param) {
        return function.apply(param);
    }

    public static <K> boolean predicateRunner(Predicate<K> function, K param) {
        return function.test(param);
    }

    public static <K, V> boolean biPredicateRunner(BiPredicate<K, V> function, K firstParam, V secondParam) {
        return function.test(firstParam, secondParam);
    }

    public static <T, M, N, L> boolean quadruplePredicateRunner(LambdaDemo.QuadruplePredicate<T, M, N, L> function,
                                                          T firstParam, M secondParam, N thirdParam, L fourthParam) {
        return function.test(firstParam, secondParam, thirdParam, fourthParam);
    }
}
