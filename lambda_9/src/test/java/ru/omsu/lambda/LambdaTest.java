package ru.omsu.lambda;

import org.junit.Assert;
import org.junit.Test;

import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdaTest {

    @Test
    public void test_getStringLength() {
        functionRunnerTest(LambdaDemo.getStringLength, "Hello world!", 12);
    }

    @Test
    public void test_getStringFirstChar() {
        functionRunnerTest(LambdaDemo.getStringFirstChar, "Hello", 'H');
    }

    @Test
    public void test_isStringNotContainsSpaces() {
        predicateRunnerTest(LambdaDemo.isStringNotContainsSpaces, "Hahaha hello", false);
    }

    @Test
    public void test_getStringWordsAmount() {
        functionRunnerTest(LambdaDemo.getStringWordsAmount, "Hello, people, hah a", 3);
        functionRunnerTest(LambdaDemo.getStringWordsAmount, "", 0);
    }

    @Test
    public void test_getHumanAge() {
        functionRunnerTest(LambdaDemo.getHumanAge, new Human("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE), 20);
        functionRunnerTest(LambdaDemo.getHumanAge,
                new Student("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE, "OMGU", "IMIT", "Proger"),
                20);
    }

    @Test
    public void test_isHumanLastNamesEquals() {
        biPredicateRunnerTest(
                LambdaDemo.isHumanLastNamesEquals,
                new Human("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE),
                new Human("Vika", "NeKaplun", "Sergeevna", 20, Sex.FEMALE),
                false
        );
        biPredicateRunnerTest(
                LambdaDemo.isHumanLastNamesEquals,
                new Human("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE),
                new Human("NeVika", "Kaplun", "Sergeevna", 20, Sex.FEMALE),
                true
        );
    }

    @Test
    public void test_getHumanFullName() {
        functionRunnerTest(
                LambdaDemo.getHumanFullName,
                new Human("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE),
                "Kaplun Vika Sergeevna"
        );

        functionRunnerTest(
                LambdaDemo.getHumanFullName,
                new Human("Hah", "", "", 20, Sex.FEMALE),
                " Hah "
        );
    }

    @Test
    public void test_setHumanOlder() {
        functionRunnerTest(
                LambdaDemo.setHumanOlder,
                new Human("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE),
                new Human("Vika", "Kaplun", "Sergeevna", 21, Sex.FEMALE)
        );
    }

    @Test
    public void test_checkHumanMaxAge() {
        quadruplePredicateRunnerTest(
                LambdaDemo.checkHumanMaxAge,
                new Human("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE),
                new Human("Vika", "Kaplun", "Sergeevna", 21, Sex.FEMALE),
                new Human("Vika", "Kaplun", "Sergeevna", 22, Sex.FEMALE),
                24,
                true
        );
        quadruplePredicateRunnerTest(
                LambdaDemo.checkHumanMaxAge,
                new Human("Vika", "Kaplun", "Sergeevna", 20, Sex.FEMALE),
                new Human("Vika", "Kaplun", "Sergeevna", 21, Sex.FEMALE),
                new Human("Vika", "Kaplun", "Sergeevna", 28, Sex.FEMALE),
                24,
                false
        );
    }

    public <K, V> void functionRunnerTest(Function<K, V> function, K param, V expected) {
        Assert.assertEquals(expected, LambdaRunner.functionRunner(function, param));
    }

    public <K> void predicateRunnerTest(Predicate<K> function, K param, boolean expected) {
        Assert.assertEquals(expected, LambdaRunner.predicateRunner(function, param));
    }

    public <K, V> void biPredicateRunnerTest(BiPredicate<K, V> function, K firstParam, V secondParam, boolean expected) {
        Assert.assertEquals(expected, LambdaRunner.biPredicateRunner(function, firstParam, secondParam));
    }

    public <T, M, N, L> void quadruplePredicateRunnerTest(LambdaDemo.QuadruplePredicate<T, M, N, L> function,
                                                          T firstParam, M secondParam, N thirdParam, L fourthParam, boolean expected) {
        Assert.assertEquals(expected, LambdaRunner.quadruplePredicateRunner(function, firstParam, secondParam, thirdParam, fourthParam));
    }
}
