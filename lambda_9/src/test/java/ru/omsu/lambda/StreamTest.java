package ru.omsu.lambda;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class StreamTest {
    @Test
    public void test_deleteNullObjects() {
        List<Object> params = new ArrayList<>();
        List<Object> expected = new ArrayList<>();
        params.add(null);
        params.add(null);
        params.add(new Object());
        params.add(new Object());
        params.add(null);

        expected.add(params.get(2));
        expected.add(params.get(3));
        testStream(StreamApiDemo.deleteNullObjects, params, expected);
    }

    @Test
    public void test_countPositiveNumbers() {
        testStream(StreamApiDemo.countPositiveNumbers,
                new HashSet<Integer>() {{
                    add(1);
                    add(-11);
                    add(12);
                    add(15);
                    add(-231);
                }}, 3L);
    }

    @Test
    public void test_getThreeLastObjects() {
        List<Object> params = new ArrayList<>();
        List<Object> expected = new ArrayList<>();
        params.add(null);
        params.add(null);
        params.add(new Object());
        params.add(new Object());
        params.add(null);

        expected.add(params.get(2));
        expected.add(params.get(3));
        expected.add(params.get(4));

        testStream(StreamApiDemo.getThreeLastObjects, params, expected);
    }

    @Test
    public void test_getFirstEvenNumber() {
        testStream(StreamApiDemo.getFirstEvenNumber, new ArrayList<Integer>() {{ add(2); add(3); add(4); }}, 2);
    }

    @Test
    public void test_getSquaredNumbers() {
        testStream(StreamApiDemo.getSquaredNumbers, new int[] {1, 2, 3}, new ArrayList<Integer>() {{ add(1); add(4); add(9); }});
    }

    @Test
    public void test_getNotEmptyStrings() {
        List<String> param = new ArrayList<>();
        param.add("");
        param.add("");
        param.add("");
        param.add("Hello");
        param.add("It's");
        param.add("Me");
        List<String> expected = new ArrayList<>();
        expected.add(param.get(3));
        expected.add(param.get(4));
        expected.add(param.get(5));
        testStream(StreamApiDemo.getNotEmptyStrings, param, expected);
    }

    @Test
    public void test_getListOfStringDest() {
        Set<String> param = new HashSet<>();
        param.add("Ange");
        param.add("Babe");
        param.add("Longe");
        param.add("Zeta");

        List<String> expected = new ArrayList<>();
        expected.add("Zeta");
        expected.add("Longe");
        expected.add("Babe");
        expected.add("Ange");
        testStream(StreamApiDemo.getListOfStringDest, param, expected);
    }

    public <K, V> void testStream(Function<K, V> function, K param, V expected) {
        Assert.assertEquals(expected, function.apply(param));
    }
}
