package ru.omsu.iostreams;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.omsu.iostreams.json.deserializaer.CustomFlatDeserializer;
import ru.omsu.iostreams.json.serializer.CustomFlatSerializer;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;


@JsonSerialize(using = CustomFlatSerializer.class)
@JsonDeserialize(using = CustomFlatDeserializer.class)
public class Flat implements Serializable {
    private int number;
    private int area;
    List<Person> people;

    public Flat(int number, int area, List<Person> people) {
        this.number = number;
        this.area = area;
        this.people = people;
    }

    public Flat() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                area == flat.area &&
                Objects.equals(people, flat.people);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, area, people);
    }

    @Override
    public String toString() {
        return "Flat{" +
                "number=" + number +
                ", area=" + area +
                ", people=" + people.stream().map(Person::toString).reduce((p1, p2) -> p1 + ", " + p2).orElse("") +
                '}';
    }
}
