package ru.omsu.iostreams.json.deserializaer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ru.omsu.iostreams.Flat;
import ru.omsu.iostreams.Person;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CustomFlatDeserializer extends StdDeserializer<Flat> {
    public CustomFlatDeserializer() {
        this(null);
    }

    protected CustomFlatDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Flat deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        ObjectMapper mapper = new ObjectMapper();

        int number = node.get("number").asInt();
        int area = node.get("area").asInt();
        List<Person> people = StreamSupport.stream(node.get("people").spliterator(), false)
                .map(p -> {
                    try {
                        return mapper.treeToValue(p, Person.class);
                    } catch (JsonProcessingException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return new Flat(number, area, people);
    }
}
