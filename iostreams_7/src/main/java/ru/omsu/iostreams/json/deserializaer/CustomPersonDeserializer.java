package ru.omsu.iostreams.json.deserializaer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ru.omsu.iostreams.Person;

import java.io.IOException;
import java.time.LocalDate;

public class CustomPersonDeserializer extends StdDeserializer<Person> {
    public CustomPersonDeserializer() {
        this(null);
    }

    protected CustomPersonDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Person deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String[] nameFields = node.get("fullName").asText().split("\\s+");
        return new Person(
                nameFields[1],
                nameFields[0],
                nameFields[2],
                LocalDate.parse(node.get("birthdayDate").asText())
        );
    }
}
