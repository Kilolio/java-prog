package ru.omsu.iostreams.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ru.omsu.iostreams.Person;

import java.io.IOException;

public class CustomPersonSerializer extends StdSerializer<Person> {

    protected CustomPersonSerializer() {
        this(null);
    }

    protected CustomPersonSerializer(Class<Person> t) {
        super(t);
    }

    @Override
    public void serialize(Person person, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("fullName",
                person.getLastName() + " " + person.getFirstName() + " " + person.getPatronymic());
        jsonGenerator.writeStringField("birthdayDate", person.getBirthdayDate().toString());
        jsonGenerator.writeEndObject();
    }
}
