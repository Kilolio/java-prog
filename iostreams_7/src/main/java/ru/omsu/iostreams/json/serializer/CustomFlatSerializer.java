package ru.omsu.iostreams.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ru.omsu.iostreams.Flat;
import ru.omsu.iostreams.Person;

import java.io.IOException;

public class CustomFlatSerializer extends StdSerializer<Flat> {

    protected CustomFlatSerializer() {
        this(null);
    }

    protected CustomFlatSerializer(Class<Flat> t) {
        super(t);
    }

    @Override
    public void serialize(Flat flat, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("number", flat.getNumber());
        jsonGenerator.writeNumberField("area", flat.getArea());
        jsonGenerator.writeArrayFieldStart("people");
        for (Person p : flat.getPeople()) {
            jsonGenerator.writeObject(p);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
