package ru.omsu.iostreams.json.deserializaer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ru.omsu.iostreams.Flat;
import ru.omsu.iostreams.House;
import ru.omsu.iostreams.Person;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CustomHouseDeserializer extends StdDeserializer<House> {
    public CustomHouseDeserializer() {
        this(null);
    }

    protected CustomHouseDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public House deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        ObjectMapper mapper = new ObjectMapper();

        String cadastralNumber = node.get("cadastralNumber").asText();
        String address = node.get("address").asText();
        Person seniorHousework = mapper.treeToValue(node.get("seniorHousework"), Person.class);
        List<Flat> flats = StreamSupport.stream(node.get("flats").spliterator(), false)
                .map(f -> {
                    try {
                        return mapper.treeToValue(f, Flat.class);
                    } catch (JsonProcessingException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        return new House(cadastralNumber, address, seniorHousework, flats);
    }
}
