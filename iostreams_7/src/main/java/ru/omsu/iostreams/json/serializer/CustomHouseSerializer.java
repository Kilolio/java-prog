package ru.omsu.iostreams.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ru.omsu.iostreams.Flat;
import ru.omsu.iostreams.House;
import ru.omsu.iostreams.Person;

import java.io.IOException;

public class CustomHouseSerializer extends StdSerializer<House> {

    protected CustomHouseSerializer() {
        this(null);
    }

    protected CustomHouseSerializer(Class<House> t) {
        super(t);
    }

    @Override
    public void serialize(House house, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("cadastralNumber", house.getCadastralNumber());
        jsonGenerator.writeStringField("address", house.getAddress());
        jsonGenerator.writeObjectField("seniorHousework", house.getSeniorHousework());
        jsonGenerator.writeArrayFieldStart("flats");
        for(Flat flat : house.getFlats()) {
            jsonGenerator.writeObject(flat);
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
