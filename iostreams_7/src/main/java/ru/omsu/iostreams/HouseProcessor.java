package ru.omsu.iostreams;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class HouseProcessor {

    public static void main(String[] args) {
        House house = new House("123456", "Omsk",
                new Person("Kostya", "Artushkevich", "Maximovich", LocalDate.now()),
                new ArrayList<Flat>() {{
                    add(new Flat(1, 100, new ArrayList<Person>() {{
                        add(new Person("Artemiy", "Lebedev", "Alexeevich", LocalDate.now()));
                    }}));
                    add(new Flat(2, 69, new ArrayList<Person>() {{
                        add(new Person("Patriarx", "Kirill", "Bogovich", LocalDate.now()));
                        add(new Person("Vadim", "Zhukov", "Mixaylov", LocalDate.now()));
                    }}));
                }});
        try {
            OutputStream os = new FileOutputStream("house");
            serialize(house, os);
            os.close();
            System.out.println("Done\n");
            InputStream is = new FileInputStream("house");
            House newHouse = deserialize(is);
            is.close();
            System.out.println(newHouse.toString());
            System.out.println(newHouse.equals(house));

            System.out.println("JSON:: ");
            os = new FileOutputStream("houseJson");
            serializeToJson(house, os);
            os.close();

            is = new FileInputStream("houseJson");
            newHouse = deserializeFromJson(is);
            System.out.println(newHouse.toString() + "\n" + house.toString() + "\n" + house.equals(newHouse));

            serializeToCsv(house);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void serialize(House house, OutputStream stream) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(stream);
        outputStream.writeObject(house);
        outputStream.flush();
    }


    public static House deserialize(InputStream stream) throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(stream);
        return (House) inputStream.readObject();
    }

    public static void serializeToJson(House house, OutputStream outputStream) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(outputStream, house);
    }

    public static House deserializeFromJson(InputStream inputStream) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(inputStream, House.class);
    }

    public static void serializeToCsv(House house) throws IOException {
        FileWriter out = new FileWriter("house_" + house.getCadastralNumber() + ".csv");
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withDelimiter(';'))) {
            printer.printRecord("Данные о доме");
            printer.println();
            printer.printRecord("Кадастровый номер:", house.getCadastralNumber());
            printer.printRecord("Адрес:", house.getAddress());
            printer.printRecord("Старший по дому:", convertFIO(house.getSeniorHousework()));
            printer.println();
            printer.printRecord("Данные о квартирах");
            printer.println();
            printer.printRecord("№", "Площадь, кв. м", "Владельцы");
            for (Flat flat : house.getFlats()) {
                printer.printRecord(
                        flat.getNumber(),
                        flat.getArea(),
                        flat.getPeople()
                                .stream()
                                .map(HouseProcessor::convertFIO)
                                .reduce((v1, v2) -> v1 + ", " + v2)
                                .orElse("")
                );
            }
        }
    }

    private static String convertFIO(Person person) {
        return person.getLastName() + " "
                + person.getFirstName().charAt(0) + ". "
                + person.getPatronymic().charAt(0) + ".";
    }
}
