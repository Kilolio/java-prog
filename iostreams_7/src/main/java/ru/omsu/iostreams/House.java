package ru.omsu.iostreams;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.omsu.iostreams.json.deserializaer.CustomHouseDeserializer;
import ru.omsu.iostreams.json.serializer.CustomHouseSerializer;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@JsonSerialize(using = CustomHouseSerializer.class)
@JsonDeserialize(using = CustomHouseDeserializer.class)
public class House implements Serializable {
    private String cadastralNumber;
    private String address;
    private Person seniorHousework;
    private List<Flat> flats;

    public House(String cadastralNumber, String address, Person seniorHousework, List<Flat> flats) {
        this.cadastralNumber = cadastralNumber;
        this.address = address;
        this.seniorHousework = seniorHousework;
        this.flats = flats;
    }

    public House() {
    }

    public String getCadastralNumber() {
        return cadastralNumber;
    }

    public void setCadastralNumber(String cadastralNumber) {
        this.cadastralNumber = cadastralNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getSeniorHousework() {
        return seniorHousework;
    }

    public void setSeniorHousework(Person seniorHousework) {
        this.seniorHousework = seniorHousework;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public void setFlats(List<Flat> flats) {
        this.flats = flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(cadastralNumber, house.cadastralNumber) &&
                Objects.equals(address, house.address) &&
                Objects.equals(seniorHousework, house.seniorHousework) &&
                Objects.equals(flats, house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cadastralNumber, address, seniorHousework, flats);
    }

    @Override
    public String toString() {

        return "House{" +
                "cadastralNumber='" + cadastralNumber + '\'' +
                ", address='" + address + '\'' +
                ", seniorHousework=" + seniorHousework.toString() +
                ", flats=" + flats.stream().map(Flat::toString).reduce("", (f1, f2) -> f1 + ", " + f2) +
                '}';
    }
}
