package ru.omsu.iostreams;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.internal.NotNull;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        try {
            OutputStream os = new FileOutputStream("output");
            writeIntArrayToBinary(new int[]{1000, 2000, 3000, 4000, 5000}, os);

            int[] array = readIntArrayFromBinary(new FileInputStream("output"));
            System.out.println(Arrays.toString(array));

            System.out.println("\n");
            writeIntArray(new int[]{5000, 40000, 3000, 20786, 1366}, new FileOutputStream("output2"));
            Integer[] arrayInt = readIntArray(new FileInputStream("output2"));
            System.out.println(Arrays.toString(arrayInt));


            System.out.println("\n\n" + Arrays.toString(readIntArrFromPosition(new File("output"), 3)));

            for (File f : getFilesInDirectoryWithExtension("csv", new File("/home/profyan/Документы/IdeaProjects/imit/novemberTask/iostreams"))) {
                System.out.println(f.getName());
            }

            for (File f : getFilesInDirectory(Pattern.compile(".*\\.(java)").pattern(), new File("/home/profyan/Документы/IdeaProjects/imit/novemberTask/iostreams"))) {
                System.out.println(f.getName());
            }

            System.out.println("Json Tree:");
            System.out.println(compareJsonStrings("{\"nick\": \"cowtowncoder\", \"hey\": \"kostya\"}", "{\"hey\": \"kostya\", \"nick\": \"cowtowncoder\"}"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeIntArrayToBinary(int array[], OutputStream stream) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(stream);
        for (int number : array) {
            dataOutputStream.writeInt(number);
        }
        dataOutputStream.flush();
    }


    public static int[] readIntArrayFromBinary(InputStream stream) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(stream);
        int[] output = new int[stream.available() / Integer.BYTES];
        int i = 0;
        while (dataInputStream.available() >= Integer.BYTES) {
            output[i++] = dataInputStream.readInt();
        }
        return output;
    }

    public static void writeIntArray(int[] array, OutputStream outputStream) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        for (int number : array) {
            outputStreamWriter.write(number + " ");
        }
        outputStreamWriter.flush();
    }

    public static Integer[] readIntArray(InputStream inputStream) throws IOException {
        Scanner scanner = new Scanner(inputStream);
        scanner.useLocale(Locale.ENGLISH);

        List<Integer> list = new ArrayList<>();
        int i = 0;
        while (scanner.hasNextInt()) {
            list.add(scanner.nextInt());
        }
        Integer[] output = new Integer[list.size()];
        return list.toArray(output);
    }

    public static int[] readIntArrFromPosition(File file, int pos) throws IOException {
        RandomAccessFile raFile = new RandomAccessFile(file, "r");
        raFile.seek(pos * Integer.BYTES);
        int[] output = new int[(int) (raFile.length() - raFile.getFilePointer()) / Integer.BYTES];
        int i = 0;
        while (raFile.getFilePointer() + Integer.BYTES <= raFile.length()) {
            output[i++] = raFile.readInt();
        }
        return output;
    }

    public static File[] getFilesInDirectoryWithExtension(String extension, File file) {
        return file.listFiles((file1, s) -> s.matches(".*\\." + extension));
    }

    public static List<File> getFilesInDirectory(String name, File file) {
        List<File> fileList = new ArrayList<>();
        File[] files = file.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.getName().matches(name)) {
                    fileList.add(f);
                }
                if (f.isDirectory()) {
                    fileList.addAll(getFilesInDirectory(name, f));
                }
            }
        }
        return fileList;
    }

    public static boolean compareJsonStrings(String firstJson, String secondJson) throws IOException {
        ObjectMapper om = new ObjectMapper();
        return om.readTree(firstJson).equals(om.readTree(secondJson));
    }
}
