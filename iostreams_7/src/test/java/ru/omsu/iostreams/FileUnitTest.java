package ru.omsu.iostreams;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileUnitTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testGetFilesInDirectoryWithExtension() throws IOException {
        File file = folder.getRoot();
        folder.newFile("Main.java");
        folder.newFile("test");
        folder.newFile("NotMain.java");
        String[] expected = new String[]{"NotMain.java", "Main.java"};
        File[] files = Main.getFilesInDirectoryWithExtension("java", file);

        Assert.assertNotNull(files);
        Assert.assertEquals(2, files.length);
        Assert.assertArrayEquals(expected, Stream.of(files).map(File::getName).toArray());
    }

    @Test
    public void testGetFilesInDirectory() {
        File file = mock(File.class);
        File file1 = mock(File.class);
        File file2 = mock(File.class);
        File file3 = mock(File.class);
        File fileSub = mock(File.class);
        File fileSub1 = mock(File.class);
        File fileSub2 = mock(File.class);

        when(file.listFiles()).thenReturn(new File[]{file1, file2});
        when(file1.listFiles()).thenReturn(new File[]{file3, fileSub});
        when(file2.listFiles()).thenReturn(new File[]{fileSub1, fileSub2});

        when(file.getName()).thenReturn("folder");
        when(file1.getName()).thenReturn("folderSrc");
        when(file2.getName()).thenReturn("folderSrc2");
        when(file3.getName()).thenReturn("Main.java");
        when(fileSub.getName()).thenReturn("project.iml");
        when(fileSub1.getName()).thenReturn("Test.java");
        when(fileSub2.getName()).thenReturn("test.csv");

        when(file.isDirectory()).thenReturn(true);
        when(file1.isDirectory()).thenReturn(true);
        when(file2.isDirectory()).thenReturn(true);
        when(file3.isDirectory()).thenReturn(false);
        when(fileSub.isDirectory()).thenReturn(false);
        when(fileSub1.isDirectory()).thenReturn(false);
        when(fileSub2.isDirectory()).thenReturn(false);

        String[] expected = new String[]{"Main.java", "Test.java"};

        List<File> files = Main.getFilesInDirectory(".*\\.java", file);
        Assert.assertNotNull(files);
        Assert.assertEquals(2, files.size());
        Assert.assertArrayEquals(expected, files.stream().map(File::getName).toArray());
    }
}
