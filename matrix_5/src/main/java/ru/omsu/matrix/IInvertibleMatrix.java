package ru.omsu.matrix;

public interface IInvertibleMatrix extends IMatrix {
    IInvertibleMatrix getInvertibleMatrix();
}
