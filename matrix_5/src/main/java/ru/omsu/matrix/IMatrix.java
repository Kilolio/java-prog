package ru.omsu.matrix;

public interface IMatrix {
    double getElement(int i, int j);

    void changeElement(int i, int j, double value);

    double getDeterminant();

    int getSize();
}
