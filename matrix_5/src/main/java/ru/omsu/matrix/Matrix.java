package ru.omsu.matrix;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class Matrix implements IMatrix, Serializable {

    private int size;
    private double[] elements;
    private double cachedDeterminant;
    private boolean isActualCache = false;

    public Matrix(IMatrix matrix) {
        this(matrix.getSize());
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = 0; j < matrix.getSize(); j++) {
                changeElement(i, j, matrix.getElement(i, j));
            }
        }
    }

    public Matrix(double values[], int size) {
        this(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                elements[i + size * j] = values[i + size * j];
            }
        }
    }

    public Matrix(int size) {
        if (size <= 0) {
            throw new MatrixException("Invalid size");
        }
        this.size = size;
        elements = new double[size * size];
    }

    @Override
    public int getSize() {
        return size;
    }

    public double getElement(int i, int j) {
        if (i < 0 || i >= size || j < 0 || j >= size) {
            throw new MatrixException("Invalid indexes: " + i + " " + j);
        }
        return elements[i * size + j];
    }

    public void changeElement(int i, int j, double value) {
        if (i < 0 || i >= size || j < 0 || j >= size) {
            throw new MatrixException("Invalid indexes: " + i + " " + j);
        }
        elements[i * size + j] = value;
        isActualCache = false;
    }

    private void swapLine(Matrix matrix, int firstLane, int secondLane) {
        double tmp;
        for (int i = 0; i < matrix.size; i++) {
            tmp = matrix.getElement(firstLane, i);
            matrix.changeElement(firstLane, i, matrix.getElement(secondLane, i));
            matrix.changeElement(secondLane, i, tmp);
        }
    }

    public double getDeterminant() {
        if (isActualCache) {
            return cachedDeterminant;
        }
        double result = 1;
        int count;


        Matrix mt = new Matrix(size);
        Matrix mt2 = new Matrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                mt.changeElement(i, j, this.getElement(i, j));
                mt2.changeElement(i, j, this.getElement(i, j));
            }
        }

        for (int i = 0; i < size; i++) {
            if (mt.getElement(i, i) == 0 && i < size - 1) {
                count = i;
                while (count < size - 1 && mt.getElement(count, i) == 0) {
                    count++;
                }

                if (mt.getElement(count, i) != 0) {
                    swapLine(mt, i, count);
                    result = -result;
                } else {
                    continue;
                }

            }

            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    mt2.changeElement(x, y, mt.getElement(x, y));
                }
            }


            for (int j = i + 1; j < size; j++) { // j - строка
                for (int k = i; k < size; k++) { // k - столбец
                    double multiplier = ((mt.getElement(i, k) * mt2.getElement(j, i)) / mt.getElement(i, i));
                    double tmp = mt.getElement(j, k) - multiplier;
                    mt.changeElement(j, k, tmp);
                }
            }

        }

        for (int x = 0; x < size; x++) {
            result = result * mt.getElement(x, x);
        }
        isActualCache = true;
        cachedDeterminant = result;
        return result;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(getSize() + "\n");
        for (int i = 0; i < getSize(); i++) {
            for (int j = 0; j < getSize(); j++) {
                stringBuilder.append(getElement(i, j)).append(" ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return size == matrix.size &&
                Arrays.equals(elements, matrix.elements);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(size);
        result = 31 * result + Arrays.hashCode(elements);
        return result;
    }
}
