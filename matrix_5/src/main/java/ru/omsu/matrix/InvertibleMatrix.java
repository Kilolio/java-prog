package ru.omsu.matrix;

public class InvertibleMatrix extends Matrix implements IInvertibleMatrix {
    public InvertibleMatrix(int size) throws MatrixException {
        super(size);
    }

    public InvertibleMatrix(IMatrix matrix) {
        super(matrix);
        if(getDeterminant() == 0) {
            throw new MatrixException("Can't create invertible matrix");
        }
    }

    public InvertibleMatrix(double[] values, int size) {
        super(values, size);
        if(getDeterminant() == 0) {
            throw new MatrixException("Can't create invertible matrix");
        }
    }

    @Override
    public IInvertibleMatrix getInvertibleMatrix() {
        double determinant = getDeterminant();
        InvertibleMatrix outputMatrix = new InvertibleMatrix(getSize());
        if (getSize() == 1) {
            outputMatrix.changeElement(0, 0, 1f / determinant);
        } else {
            for (int i = 0; i < getSize(); i++) {
                for (int j = 0; j < getSize(); j++) {
                    outputMatrix.changeElement(i, j, getMinor(i, j) * (((i + j) % 2 == 0) ? 1 : -1) / determinant);
                }
            }
        }
        return transpositionMatrix(outputMatrix);
    }

    private IInvertibleMatrix transpositionMatrix(IInvertibleMatrix matrix) {
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = i + 1; j < matrix.getSize(); j++) {
                double temp = matrix.getElement(i, j);
                matrix.changeElement(i, j, matrix.getElement(j, i));
                matrix.changeElement(j, i, temp);
            }
        }
        return matrix;
    }

    private double getMinor(int str, int col) {
        IMatrix tmpMatrix;
        tmpMatrix = new Matrix(getSize() - 1);
        int o = 0, p = 0;
        for (int i = 0; i < getSize(); i++) {
            if (i != str) {
                p = 0;
                for (int j = 0; j < getSize(); j++) {
                    if (j != col) {
                        tmpMatrix.changeElement(o, p, getElement(i, j));
                        p++;
                    }
                }
                o++;
            }
        }
        return tmpMatrix.getDeterminant();
    }

}
