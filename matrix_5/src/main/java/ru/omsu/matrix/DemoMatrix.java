package ru.omsu.matrix;

import java.io.*;
import java.util.Locale;
import java.util.Scanner;

public class DemoMatrix {
    public static void main(String[] args) {
        try {
            final int size = 3;
            IMatrix matrix = new Matrix(size);
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    matrix.changeElement(i, j, 2);
                }
            }
            System.out.println("Determinant: " + matrix.getDeterminant());
            OutputStream outputStream = new FileOutputStream("output");
            printMatrix(matrix, outputStream);
            outputStream.close();
            InputStream inputStream = new FileInputStream("output");
            Matrix inputMatix = readMatrix(inputStream);
            inputStream.close();
            printMatrix(inputMatix, System.out);
            System.out.println("isEquals: " + (matrix.equals(inputMatix)));

            serializeAndWrite(matrix, new FileOutputStream("output2"));
            Matrix deserializeMatrix = deserializeAndRead(new FileInputStream("output2"));
            System.out.println("Is deserialized matrix equals: " + matrix.equals(deserializeMatrix));

            IInvertibleMatrix iMatrix = new InvertibleMatrix(new double[]{2, 5, 7, 6, 3, 4, 5, -2, -3}, 3);
            IInvertibleMatrix invert = iMatrix.getInvertibleMatrix();
            System.out.println("Invertible matrix: " + invert.toString() + "\n" + "Matrix: " + iMatrix.toString() + "\n" + "Invert invert: " +
                    invert.getInvertibleMatrix().toString() + " IsEquals: " + iMatrix.equals(invert.getInvertibleMatrix()));

            Matrix zeroMatrix = new Matrix(3);
            zeroMatrix.changeElement(0, 0, 0);
            zeroMatrix.changeElement(0, 1, 5);
            zeroMatrix.changeElement(0, 2, 7);
            zeroMatrix.changeElement(1, 0, 0);
            zeroMatrix.changeElement(1, 1, 0);
            zeroMatrix.changeElement(1, 2, 4);
            zeroMatrix.changeElement(2, 0, 0);
            zeroMatrix.changeElement(2, 1, -2);
            zeroMatrix.changeElement(2, 2, 0);
            System.out.println("Det of zeroMatrix: " + zeroMatrix.toString() + "\n" + zeroMatrix.getDeterminant());

        } catch (MatrixException | ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void printMatrix(IMatrix matrix, OutputStream outputStream) throws IOException {
        outputStream.write(matrix.toString().getBytes());
        outputStream.flush();
    }

    public static Matrix readMatrix(InputStream stream) {
        Scanner scanner = new Scanner(stream);
        scanner.useLocale(Locale.ENGLISH);
        int size = scanner.nextInt();
        Matrix matrix = new Matrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix.changeElement(i, j, scanner.nextDouble());
            }
        }
        scanner.close();
        return matrix;
    }

    public static double getSummaryMatrix(Matrix matrix) {
        double sum = 0d;
        for (int i = 0; i < matrix.getSize(); i++) {
            for (int j = 0; j < matrix.getSize(); j++) {
                sum += matrix.getElement(i, j);
            }
        }
        return sum;
    }

    public static void serializeAndWrite(IMatrix matrix, OutputStream outputStream) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(outputStream)) {
            oos.writeObject(matrix);
        }
    }

    public static Matrix deserializeAndRead(InputStream inputStream) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(inputStream)) {
            return (Matrix) ois.readObject();
        }
    }
}
