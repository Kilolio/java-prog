package ru.omsu;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.omsu.collections.*;

import java.util.*;

@RunWith(JUnit4.class)
public class CollectionsDemoTest {

    @Test
    public void testGetStringsStartsWithSymbolAmount() {
        List<String> stringList = new ArrayList<>();
        stringList.add("Viktoriya");
        stringList.add("Viktor");
        stringList.add("Viking");
        stringList.add("Lektory");
        stringList.add("Lizavi");
        Assert.assertEquals(3, CollectionsDemo.getStringsStartsWithSymbolAmount(stringList, 'V'));
        Assert.assertEquals(0, CollectionsDemo.getStringsStartsWithSymbolAmount(new ArrayList<>(), 'v'));
    }

    @Test
    public void testGetHumanWithSameLastName() {
        List<Human> humanList = new ArrayList<>();
        humanList.add(new Human("Viktoriya", "Kaplun", "Sergeevna", 19));
        humanList.add(new Human("Elena", "Kaplun", "Sergeevna", 30));
        humanList.add(new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanList.add(new Human("Vadim", "Zhukov", "Sergeevich", 18));

        Human selectedHuman = new Human(humanList.get(0));
        List<Human> outputList = CollectionsDemo.getHumanWithSameLastName(humanList, selectedHuman);
        Assert.assertTrue(outputList.contains(humanList.get(0)));
        Assert.assertTrue(outputList.contains(humanList.get(1)));
        Assert.assertTrue(outputList.contains(humanList.get(2)));
        Assert.assertEquals(3, outputList.size());
    }

    @Test
    public void testGetHumanWithoutSelected() {
        List<Human> humanList = new ArrayList<>();
        humanList.add(new Human("Viktoriya", "Kaplun", "Sergeevna", 19));
        humanList.add(new Human("Elena", "Kaplun", "Sergeevna", 30));
        humanList.add(new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanList.add(new Human("Vadim", "Zhukov", "Sergeevich", 18));

        Human selectedHuman = new Human(humanList.get(0));
        List<Human> outputList = CollectionsDemo.getHumanWithoutSelected(humanList, selectedHuman);
        Assert.assertTrue(outputList.contains(humanList.get(1)));
        Assert.assertTrue(outputList.contains(humanList.get(2)));
        Assert.assertTrue(outputList.contains(humanList.get(3)));
        Assert.assertFalse(outputList.contains(humanList.get(0)));
        Assert.assertEquals(3, outputList.size());
    }

    @Test
    public void testGetSetOfIntegerWithoutIntersection() {
        List<Set<Integer>> setList = new ArrayList<>();
        setList.add(new HashSet<Integer>() {{
            addAll(Arrays.asList(1, 2, 3));
        }});
        setList.add(new HashSet<Integer>() {{
            addAll(Arrays.asList(4, 5, 6));
        }});
        setList.add(new HashSet<Integer>() {{
            addAll(Arrays.asList(7, 8, 9));
        }});

        Set<Integer> selectedSet = new HashSet<>(Arrays.asList(6, 7, 8));
        List<Set<Integer>> outputList = CollectionsDemo.getSetOfIntegerWithoutIntersection(setList, selectedSet);
        Assert.assertEquals(1, outputList.size());
        Assert.assertEquals(setList.get(0), outputList.get(0));
    }

    @Test
    public void testGetHumanWithMaxAge() {
        List<Human> humanList = new ArrayList<>();
        humanList.add(new Human("Viktoriya", "Kaplun", "Sergeevna", 19));
        humanList.add(new Human("Elena", "Kaplun", "Sergeevna", 30));
        humanList.add(new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanList.add(new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanList.add(new Student("Kostya", "Kirkorov", "Vladislavovich", 31, "Imit"));
        humanList.add(new Human("Vadim", "Zhukov", "Sergeevich", 30));

        Set<? extends Human> humans = CollectionsDemo.getHumanWithMaxAge(humanList);
        Assert.assertEquals(2, humans.size());
        Assert.assertTrue(humans.contains(humanList.get(4)));
        Assert.assertTrue(humans.contains(humanList.get(2)));
    }

    @Test
    public void testGetSelectedHumanSet() {
        Map<Integer, Human> humanList = new HashMap<>();
        humanList.put(1, new Human("Viktoriya", "Kaplun", "Sergeevna", 19));
        humanList.put(2, new Human("Elena", "Kaplun", "Sergeevna", 30));
        humanList.put(3, new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanList.put(4, new Student("Kostya", "Kirkorov", "Vladislavovich", 31, "Imit"));
        humanList.put(5, new Human("Vadim", "Zhukov", "Sergeevich", 30));

        Set<Integer> selected = new HashSet<>(Arrays.asList(1, 3, 5));

        Set<Human> humans = CollectionsDemo.getSelectedHumanSet(humanList, selected);
        Assert.assertEquals(3, humans.size());
        Assert.assertTrue(humans.contains(humanList.get(1)));
        Assert.assertTrue(humans.contains(humanList.get(3)));
        Assert.assertTrue(humans.contains(humanList.get(5)));
    }

    @Test
    public void testGetHumanIdentificators() {
        Map<Integer, Human> humanList = new HashMap<>();
        humanList.put(1, new Human("Viktoriya", "Kaplun", "Sergeevna", 19));
        humanList.put(2, new Human("Elena", "Kaplun", "Sergeevna", 17));
        humanList.put(3, new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanList.put(4, new Student("Kostya", "Kirkorov", "Vladislavovich", 31, "Imit"));
        humanList.put(5, new Human("Vadim", "Zhukov", "Sergeevich", 14));

        List<Integer> selected = new ArrayList<>(Arrays.asList(1, 3, 4));
        List<Integer> outputList = CollectionsDemo.getHumanIdentificators(humanList);
        Assert.assertEquals(selected, outputList);
    }

    @Test
    public void testGetAgesForIdentificators() {
        Map<Integer, Human> humanList = new HashMap<>();
        humanList.put(1, new Human("Viktoriya", "Kaplun", "Sergeevna", 19));
        humanList.put(2, new Human("Elena", "Kaplun", "Sergeevna", 17));
        humanList.put(3, new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanList.put(4, new Student("Kostya", "Kirkorov", "Vladislavovich", 31, "Imit"));
        humanList.put(5, new Human("Vadim", "Zhukov", "Sergeevich", 14));

        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(1, 19);
        expected.put(2, 17);
        expected.put(3, 31);
        expected.put(4, 31);
        expected.put(5, 14);
        Map<Integer, Integer> outputMap = CollectionsDemo.getAgesForIdentificators(humanList);
        Assert.assertEquals(expected, outputMap);
    }

    @Test
    public void testGetHumanForAgeIdentificator() {
        List<Human> humanSet = new ArrayList<>();
        humanSet.add(new Human("Viktoriya", "Kaplun", "Sergeevna", 19));
        humanSet.add(new Human("Elena", "Kaplun", "Sergeevna", 30));
        humanSet.add(new Human("Sergey", "Kaplun", "Sergeevich", 31));
        humanSet.add(new Student("Kostya", "Kirkorov", "Vladislavovich", 31, "Imit"));
        humanSet.add(new Human("Vadim", "Zhukov", "Sergeevich", 30));

        Map<Integer, Set<Human>> expected = new HashMap<>();
        expected.put(19, new HashSet<Human>() {{ add(humanSet.get(0)); }});
        expected.put(30, new HashSet<Human>() {{ add(humanSet.get(1)); add(humanSet.get(4)); }});
        expected.put(31, new HashSet<Human>() {{ add(humanSet.get(2)); add(humanSet.get(3)); }});

        Map<Integer, Set<Human>> output = CollectionsDemo.getHumanForAgeIdentificator(new HashSet<>(humanSet));
        Assert.assertEquals(expected, output);

    }


    @Test
    public void testGetSortedHumanList() {
        List<? extends Human> students = new ArrayList<Human>() {{
            add(new Student("Kostya", "Artyshkevich", "Maximovich", 19, "Imit"));
            add(new Student("Petya", "Karimov", "Petrov", 18, "Imit"));
            add(new Student("Sonya", "Kokovsky", "Fedorovna", 18, "Imit"));
            add(new Student("Olesya", "Kokovsky", "Fedorovna", 19, "Imit"));
        }};
        List<? extends Human> humans = CollectionsDemo.getSortedHumanList(new HashSet<>(students));
        Assert.assertEquals(students.size(), humans.size());
        Assert.assertEquals(students.get(0), humans.get(3));
        Assert.assertEquals(students.get(1), humans.get(2));
        Assert.assertEquals(students.get(2), humans.get(0));
        Assert.assertEquals(students.get(3), humans.get(1));
    }

    @Test
    public void testGetSortedHumanMapByAgeAndName() {
        List<Human> students = new ArrayList<Human>() {{
            add(new Student("Kostya", "Artyshkevich", "Maximovich", 19, "Imit"));
            add(new Student("Petya", "Karimov", "Petrov", 18, "Imit"));
            add(new Student("Vika", "Kaplun", "Sergeevna", 19, "Imit"));
            add(new Student("Vika", "Nekaplun", "Fedorovna", 18, "Imit"));
        }};
        Map<Integer, Map<Character, List<Human>>> expected = new HashMap<>();
        expected.put(18, new HashMap<Character, List<Human>>() {{
            put('A', new ArrayList<>());
            put('K', Collections.singletonList(students.get(1)));
            put('N', Collections.singletonList(students.get(3)));
        }});
        expected.put(19, new HashMap<Character, List<Human>>() {{
            put('A', Collections.singletonList(students.get(0)));
            put('K',  Collections.singletonList(students.get(2)));
            put('N', new ArrayList<>());
        }});

        Map<Integer, Map<Character, List<Human>>> output = CollectionsDemo.getSortedHumanMapByAgeAndName(new HashSet<>(students));
        Assert.assertEquals(expected, output);

    }

}
