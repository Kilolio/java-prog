package ru.omsu;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.omsu.collections.Human;
import ru.omsu.collections.PhoneBook;

import java.util.*;

@RunWith(JUnit4.class)
public class PhoneBookTest {

    @Test
    public void testGetHumanByPhone() {
        PhoneBook phoneBook = new PhoneBook();
        Human h = new Human("Kostya", "Bog", "kostinkov", 19);
        phoneBook.addPhone(h, "123456");
        phoneBook.addPhone(new Human("first", "second", "third", 12), "543216");
        phoneBook.addPhone(new Human("first", "second", "third", 15), "232524");
        Human find = phoneBook.getHumanByPhone("123456");
        Assert.assertEquals(h, find);
    }

    @Test
    public void testGetHumanPhonesMapByLastName() {
        PhoneBook phoneBook = new PhoneBook();
        List<Human> humanList = new ArrayList<>();
        humanList.add(new Human("first", "Agapov", "third", 12));
        humanList.add(new Human("first", "Agarkov", "third", 32));
        humanList.add(new Human("first", "Pervih", "third", 42));
        humanList.add(new Human("first", "Pelsh", "third", 82));
        humanList.add(new Human("first", "Quartinov", "third", 11));

        phoneBook.addPhone(humanList.get(0), "123456");
        phoneBook.addPhone(humanList.get(0), "654321");
        phoneBook.addPhone(humanList.get(1), "876543");
        phoneBook.addPhone(humanList.get(2), "+79650");
        phoneBook.addPhone(humanList.get(3), "873023");
        phoneBook.addPhone(humanList.get(4), "890932");


        Map<Human, List<String>> expected = new HashMap<>();
        expected.put(humanList.get(0), Arrays.asList("123456", "654321"));
        expected.put(humanList.get(1), Collections.singletonList("876543"));
        Map<Human, List<String>> output = phoneBook.getHumanPhonesMapByLastName("Aga");
        Assert.assertEquals(expected, output);

        expected.clear();
        expected.put(humanList.get(0), Arrays.asList("123456", "654321"));
        output = phoneBook.getHumanPhonesMapByLastName("Agap");
        Assert.assertEquals(expected, output);

        expected.clear();
        expected.put(humanList.get(2), Collections.singletonList("+79650"));
        expected.put(humanList.get(3), Collections.singletonList("873023"));
        output = phoneBook.getHumanPhonesMapByLastName("P");
        Assert.assertEquals(expected, output);
    }
}
