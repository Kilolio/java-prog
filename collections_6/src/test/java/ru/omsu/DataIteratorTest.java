package ru.omsu;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.omsu.collections.Data;
import ru.omsu.collections.DataDemo;
import ru.omsu.collections.Group;

import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class DataIteratorTest {

    @Test
    public void testGetAll() {
        Data data = new Data("TestData", new Group(1, 1, 2, 3, 4), new Group(2, 5, 6, 7, 8),
                new Group(3, 9, 10, 11, 12));
        List<Integer> expected = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
        List<Integer> actual = DataDemo.getAll(data);
        Assert.assertEquals(expected, actual);
    }
}
