package ru.omsu.collections;

public class Student extends Human {

    private String faculty;

    public Student(String firstName, String lastName, String patronymic, int age, String faculty) {
        super(firstName, lastName, patronymic, age);
        this.faculty = faculty;
    }

    public Student(Human human, String faculty) {
        super(human);
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }



}
