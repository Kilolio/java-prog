package ru.omsu.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PhoneBook {
    private Map<Human, List<String>> phones;

    public PhoneBook() {
        phones = new HashMap<>();
    }

    public void addPhone(Human human, String phone) {
        phones.compute(human, (human1, phoneList) -> {
            if (phoneList == null) {
                List<String> output = new ArrayList<>();
                output.add(phone);
                return output;
            } else {
                phoneList.add(phone);
            }
            return phoneList;
        });
    }

    public void deletePhone(Human human, String phone) {
        phones.computeIfPresent(human, ((human1, strings) -> {
            strings.remove(phone);
            return strings;
        }));
    }

    public List<String> getPhonesByHuman(Human human) {
        return phones.get(human);
    }

    public Human getHumanByPhone(String phone) {
        return phones
                .entrySet()
                .stream()
                .filter((entry) -> entry.getValue().contains(phone))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(null);
    }

    public Map<Human, List<String>> getHumanPhonesMapByLastName(String lastNamePart) {
        return phones
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().getLastName().startsWith(lastNamePart))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
