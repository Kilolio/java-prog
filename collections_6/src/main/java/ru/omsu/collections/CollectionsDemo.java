package ru.omsu.collections;

import java.util.*;
import java.util.stream.Collectors;

public class CollectionsDemo {
    public static long getStringsStartsWithSymbolAmount(List<String> strings, char start) {
        return strings
                .stream()
                .filter(string -> string.startsWith(String.valueOf(start)))
                .count();
    }

    public static List<Human> getHumanWithSameLastName(List<Human> humans, Human selectedHuman) {
        return humans
                .stream()
                .filter(human -> human.getLastName().equals(selectedHuman.getLastName()))
                .collect(Collectors.toList());
    }

    public static List<Human> getHumanWithoutSelected(List<Human> humans, Human selectedHuman) {
        return humans
                .stream()
                .filter(human -> !human.equals(selectedHuman))
                .map(Human::new)
                .collect(Collectors.toList());
    }

    public static List<Set<Integer>> getSetOfIntegerWithoutIntersection(List<Set<Integer>> setList, Set<Integer> integerSet) {
        return setList
                .stream()
                .filter(set -> Collections.disjoint(set, integerSet))
                .collect(Collectors.toList());
    }

    public static Set<? extends Human> getHumanWithMaxAge(List<? extends Human> humans) {
        return humans
                .stream()
                .collect(Collectors.groupingBy(Human::getAge, TreeMap::new, Collectors.toSet()))
                .lastEntry()
                .getValue();
    }

    public static Set<Human> getSelectedHumanSet(Map<Integer, Human> humanMap, Set<Integer> selected) {
        return humanMap
                .entrySet()
                .stream()
                .filter(entry -> selected.contains(entry.getKey()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

    public static List<Integer> getHumanIdentificators(Map<Integer, Human> humanMap) {
        return humanMap
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().getAge() >= 18)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public static Map<Integer, Integer> getAgesForIdentificators(Map<Integer, Human> humanMap) {
        return humanMap
                .entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().getAge()));
    }

    public static Map<Integer, Set<Human>> getHumanForAgeIdentificator(Set<Human> humans) {
        return humans
                .stream()
                .collect(Collectors.toMap(
                        Human::getAge,
                        human -> humans
                                .stream()
                                .filter(hm -> hm.getAge() == human.getAge())
                                .collect(Collectors.toSet()),
                        (first, second) -> second
                        )
                );
    }

    //****

    public static List<? extends Human> getSortedHumanList(Set<? extends Human> humans) {
        TreeSet<Human> humansTree = new TreeSet<>(
                Comparator
                        .comparing((Human human) -> human.getLastName() + human.getFirstName() + human.getPatronymic())
                        .reversed()
        );
        humansTree.addAll(humans);
        return new ArrayList<>(humansTree);
    }

    public static Map<Integer, Map<Character, List<Human>>> getSortedHumanMapByAgeAndName(Set<Human> humans) {
        return humans
                .stream()
                .collect(Collectors.toMap(
                        Human::getAge,
                        human1 -> humans
                                .stream()
                                .collect(Collectors.toMap(
                                        h -> h.getLastName().charAt(0),
                                        human2 -> humans
                                                .stream()
                                                .filter(h -> h.getAge() == human1.getAge() &&
                                                        h.getLastName().charAt(0) == human2.getLastName().charAt(0))
                                                .sorted(Comparator.comparing((Human h) -> h.getLastName() + h.getFirstName() + h.getPatronymic()))
                                                .collect(Collectors.toList()),
                                        (first, second) -> second)

                                ),
                        (first, second) -> second
                        )
                );
    }


}
