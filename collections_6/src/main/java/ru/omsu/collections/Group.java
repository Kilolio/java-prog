package ru.omsu.collections;

public class Group {
    private int id;
    private int[] numbers;

    public Group(int id, int size) {
        this(id, new int[size]);
    }

    public Group(int id, int... numbers) {
        this.id = id;
        this.numbers = numbers;
    }

    public int getSize() {
        return numbers.length;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getNumbers() {
        return numbers;
    }

    public void setNumbers(int[] numbers) {
        this.numbers = numbers;
    }
}
