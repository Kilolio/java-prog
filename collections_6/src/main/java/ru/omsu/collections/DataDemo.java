package ru.omsu.collections;

import java.util.ArrayList;
import java.util.List;

public class DataDemo {
    public static List<Integer> getAll(Data data) {
        List<Integer> outputList = new ArrayList<>();
        for (Integer eData : data) {
            outputList.add(eData);
        }
        return outputList;
    }
}
