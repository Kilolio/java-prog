package ru.omsu.collections;

import java.util.Iterator;
import java.util.function.Consumer;

public class DataIterator implements Iterator<Integer> {

    private Data data;
    private int groupIndex;
    private int numberIndex;

    public DataIterator(Data data) {
        this.data = data;
        groupIndex = 0;
        numberIndex = 0;
    }

    @Override
    public boolean hasNext() {
        return groupIndex < data.getSize() - 1 || (groupIndex < data.getSize() && numberIndex < data.getGroups()[groupIndex].getSize());
    }

    @Override
    public Integer next() {
        int outputValue  = data.getGroups()[groupIndex].getNumbers()[numberIndex];
        if (numberIndex == data.getGroups()[groupIndex].getSize() - 1) {
            numberIndex = 0;
            groupIndex++;
        } else {
            numberIndex++;
        }
        return outputValue;
    }
}
