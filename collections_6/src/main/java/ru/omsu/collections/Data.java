package ru.omsu.collections;

import java.util.Iterator;

public class Data implements Iterable<Integer> {
    private String name;
    private Group[] groups;


    public Data(String name, Group... groups) {
        this.name = name;
        this.groups = groups;
    }

    public int getSize() {
        return groups.length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(Group[] groups) {
        this.groups = groups;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new DataIterator(this);
    }
}
