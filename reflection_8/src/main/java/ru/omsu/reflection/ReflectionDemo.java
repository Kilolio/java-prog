package ru.omsu.reflection;

import ru.omsu.lambda.Human;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ReflectionDemo {
    public static long getClassAmountExtendsHuman(List<?> objectList) {
        return objectList
                .stream()
                .filter(object -> object instanceof Human)
                .count();
    }

    public static List<String> getClassMethodsNameList(Object object) {
        return Arrays
                .stream(object.getClass().getMethods())
                .map(Method::getName)
                .collect(Collectors.toList());

    }

    public static List<String> getClassSuperclassNameList(Object object) {
        Class superClass = object.getClass().getSuperclass();
        List<String> output = new ArrayList<>();
        while (superClass != null) {
            output.add(superClass.getName());
            superClass = superClass.getSuperclass();
        }
        return output;
    }

    public static long getClassImplementsExecutableCount(List<?> objects) {
        return objects
                .stream()
                .filter(object -> Executable.class.isAssignableFrom(object.getClass()))
                .peek(exec -> ((Executable)exec).execute())
                .count();
    }

    public static List<String> getClassSettersAndGettersNameList(Object object) {
        return Arrays
                .stream(object.getClass().getMethods())
                .filter(method -> !Modifier.isStatic(method.getModifiers()) && ((method.getParameterCount() == 0
                        && (method.getName().startsWith("get") || method.getName().startsWith("is"))
                        && !method.getReturnType().equals(void.class)) ||
                        (method.getName().startsWith("set") && method.getParameterCount() == 1
                        && method.getReturnType().equals(void.class))))
                .map(Method::getName)
                .collect(Collectors.toList());
    }


}
