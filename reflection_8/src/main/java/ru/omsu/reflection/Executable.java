package ru.omsu.reflection;

public interface Executable {
    void execute();
}
