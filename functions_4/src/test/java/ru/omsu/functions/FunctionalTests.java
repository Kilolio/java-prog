package ru.omsu.functions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.omsu.functional.impl.IntegralFunctional;
import ru.omsu.functional.impl.SumFunctional;
import ru.omsu.functions.exception.InvalidArgumentException;
import ru.omsu.functions.impl.LinearFunction;
import ru.omsu.functions.impl.TrigonometricFunction;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class FunctionalTests {
    @DataProvider
    public static Object[][] linearEquationsFunctional() {
        return new Object[][]{
                {1d, -2d, 0d, 3d, -1.5d},
                {1d, 5d, 0d, 8d, 27d}
        };
    }

    @DataProvider
    public static Object[][] linearEquationsFunctionalException() {
        return new Object[][]{
                {1d, -2d, 0d, -1d},
                {1d, 5d, 0d, -2d}
        };
    }

    @DataProvider
    public static Object[][] trigonometricEquationsIntegral() {
        return new Object[][]{
                {1d, -2d, 0d, 2d, 1d, 2d, -0.11875d},
                {2d, 5d, 1d, 3d, 1d, 3d, 0.41734}
        };
    }

    @DataProvider
    public static Object[][] trigonometricEquationsIntegralException() {
        return new Object[][]{
                {1d, -2d, 0d, 2d, 1d, 3d},
                {2d, 5d, 0d, 3d, 1d, 0d}
        };
    }

    @Test(dataProvider = "linearEquationsFunctional")
    public void testLinearFunctions(double a, double b, double minInterval, double maxInterval, double expected)
            throws InvalidArgumentException {
        checkLinearFunctional(a, b, minInterval, maxInterval, expected);
    }

    @Test(dataProvider = "linearEquationsFunctionalException", expectedExceptions = {InvalidArgumentException.class})
    public void testLinearFunctions_expectedException(double a, double b, double minInterval, double maxInterval)
            throws InvalidArgumentException {
        checkLinearFunctional(a, b, minInterval, maxInterval, 0d);
        fail();
    }

    @Test(dataProvider = "trigonometricEquationsIntegral")
    public void trigonometricEquationsIntegral(double a, double b, double minInterval, double maxInterval,
                                               double integralA, double integralB, double expected)
            throws InvalidArgumentException {
        checkTrigonometricIntegral(a, b, minInterval, maxInterval, expected, integralA, integralB);
    }

    @Test(dataProvider = "trigonometricEquationsIntegralException", expectedExceptions = {InvalidArgumentException.class})
    public void trigonometricEquationsIntegral_expectedException(double a, double b, double minInterval, double maxInterval,
                                               double integralA, double integralB)
            throws InvalidArgumentException {
        checkTrigonometricIntegral(a, b, minInterval, maxInterval, 0d, integralA, integralB);
    }

    private void checkLinearFunctional(double a, double b, double minInterval, double maxInterval, double expected)
            throws InvalidArgumentException {
        FunctionWithOneArgument linearFunction = new LinearFunction(a, b, minInterval, maxInterval);
        SumFunctional<FunctionWithOneArgument> sumFunctional = new SumFunctional<FunctionWithOneArgument>();
        assertEquals(sumFunctional.calculate(linearFunction), expected, 0.0001d);
    }

    private void checkTrigonometricIntegral(double a, double b, double minInterval, double maxInterval, double expected,
                                            double integralA, double integralB)
            throws InvalidArgumentException {
        FunctionWithOneArgument trigonometricFunction = new TrigonometricFunction(a, b, minInterval, maxInterval);
        IntegralFunctional<FunctionWithOneArgument> integralFunctional
                = new IntegralFunctional<FunctionWithOneArgument>(integralA, integralB);
        assertEquals(integralFunctional.calculate(trigonometricFunction), expected, 0.1d);
    }

}
