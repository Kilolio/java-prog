package ru.omsu.functions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.omsu.functions.exception.InvalidArgumentException;
import ru.omsu.functions.impl.DifficultFunction;
import ru.omsu.functions.impl.ExponentialFunction;
import ru.omsu.functions.impl.LinearFunction;
import ru.omsu.functions.impl.TrigonometricFunction;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class FunctionsTests {
    @DataProvider
    public static Object[][] linearEquations() {
        return new Object[][]{
                {1d, -2d, 0d, 3d, 2d, 0d},
                {1d, 5d, 0d, 8d, 0d, 5d}
        };
    }

    @DataProvider
    public static Object[][] linearEquationsException() {
        return new Object[][]{
                {1d, -2d, 0d, 3d, 4d},
                {1d, 5d, 0d, -2d, 3d}
        };
    }

    @DataProvider
    public static Object[][] trigonometricEquations() {
        return new Object[][]{
                {1d, 1d, 0d, 3d, Math.PI/2d, 1d},
                {1d, 5d, 0d, 8d, 0d, 0d}
        };
    }

    @DataProvider
    public static Object[][] trigonometricEquationsException() {
        return new Object[][]{
                {1d, -2d, 0d, 3d, 4d},
                {1d, 5d, 0d, -2d, 3d}
        };
    }

    @DataProvider
    public static Object[][] difficultEquations() {
        return new Object[][]{
                {1d, 2d, 3d, 1d, 2d, 6d, 3d, 0.5d},
                {1d, 1d, 1d, 1d, 0d, 6d, 1d, 1d}
        };
    }

    @DataProvider
    public static Object[][] difficultEquationsException() {
        return new Object[][]{
                {1d, 2d, 3d, -3d, 1d, 6d, 1d},
                {1d, 1d, 1d, 1d, 0d, 6d, 8d}
        };
    }

    @DataProvider
    public static Object[][] exponentialEquations() {
        return new Object[][]{
                {1d, 2d, 0d, 6d, 1d, 2 + Math.E},
                {5d, -2d, 0d, 6d, 0d, 3d}
        };
    }

    @DataProvider
    public static Object[][] exponentialEquationsException() {
        return new Object[][]{
                {1d, 2d, 3d, 6d, 1d},
                {1d, 1d, 1d, 6d, 8d}
        };
    }


    @Test(dataProvider = "linearEquations")
    public void testLinearFunctions(double a, double b, double minInterval, double maxInterval, double arg, double expected)
            throws InvalidArgumentException {
        checkLinearFunction(a, b, minInterval, maxInterval, arg, expected);
    }

    @Test(dataProvider = "linearEquationsException", expectedExceptions = {InvalidArgumentException.class})
    public void testLinearFunctions_expectedException(double a, double b, double minInterval, double maxInterval, double arg)
            throws InvalidArgumentException {
        checkLinearFunction(a, b, minInterval, maxInterval, arg, 0d);
        fail();
    }

    @Test(dataProvider = "trigonometricEquations")
    public void testTrigonometricFunctions(double a, double b, double minInterval, double maxInterval, double arg, double expected)
            throws InvalidArgumentException {
        checkTrigonometricFunction(a, b, minInterval, maxInterval, arg, expected);
    }

    @Test(dataProvider = "trigonometricEquationsException", expectedExceptions = {InvalidArgumentException.class})
    public void testTrigonometricFunctions_expectedException(double a, double b, double minInterval, double maxInterval, double arg)
            throws InvalidArgumentException {
        checkTrigonometricFunction(a, b, minInterval, maxInterval, arg, 0d);
        fail();
    }

    @Test(dataProvider = "difficultEquations")
    public void testDifficultFunctions(double a, double b, double c, double d, double minInterval, double maxInterval,
                                       double arg, double expected) throws InvalidArgumentException {
        checkDifficultFunction(a, b, c, d, minInterval, maxInterval, arg, expected);
    }

    @Test(dataProvider = "difficultEquationsException", expectedExceptions = {InvalidArgumentException.class})
    public void testDifficultFunctions_expectedException(double a, double b, double c, double d, double minInterval,
                                                         double maxInterval, double arg) throws InvalidArgumentException {
        checkDifficultFunction(a, b, c, d, minInterval, maxInterval, arg, 0d);
        fail();
    }

    @Test(dataProvider = "exponentialEquations")
    public void testExponentialFunctions(double a, double b, double minInterval, double maxInterval, double arg, double expected)
            throws InvalidArgumentException {
        checkExponentialFunction(a, b, minInterval, maxInterval, arg, expected);
    }

    @Test(dataProvider = "exponentialEquationsException", expectedExceptions = {InvalidArgumentException.class})
    public void testExponentialFunctions_expectedException(double a, double b, double minInterval, double maxInterval, double arg)
            throws InvalidArgumentException {
        checkExponentialFunction(a, b, minInterval, maxInterval, arg, 0d);
        fail();
    }

    private void checkExponentialFunction(double a, double b, double minInterval, double maxInterval,
                                        double arg, double expected) throws InvalidArgumentException {
        FunctionWithOneArgument exponentialFunction = new ExponentialFunction(a, b, minInterval, maxInterval);
        assertEquals(exponentialFunction.calculate(arg), expected);
    }

    private void checkDifficultFunction(double a, double b, double c, double d, double minInterval, double maxInterval,
                                        double arg, double expected) throws InvalidArgumentException {
        FunctionWithOneArgument difficultFunction = new DifficultFunction(a, b, minInterval, maxInterval, c, d);
        assertEquals(difficultFunction.calculate(arg), expected);
    }

    private void checkTrigonometricFunction(double a, double b, double minInterval, double maxInterval, double arg, double expected)
            throws InvalidArgumentException {
        FunctionWithOneArgument trigonometricFunction = new TrigonometricFunction(a, b, minInterval, maxInterval);
        assertEquals(trigonometricFunction.calculate(arg), expected);
    }

    private void checkLinearFunction(double a, double b, double minInterval, double maxInterval, double arg, double expected)
            throws InvalidArgumentException {
        FunctionWithOneArgument linearFunction = new LinearFunction(a, b, minInterval, maxInterval);
        assertEquals(linearFunction.calculate(arg), expected);
    }
}
