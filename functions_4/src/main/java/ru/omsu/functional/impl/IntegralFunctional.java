package ru.omsu.functional.impl;

import ru.omsu.functional.Functional;
import ru.omsu.functions.FunctionWithOneArgument;
import ru.omsu.functions.exception.InvalidArgumentException;

public class IntegralFunctional<T extends FunctionWithOneArgument> implements Functional<T> {

    private double minIntervalIntegration;
    private double maxIntervalIntegration;

    private static final double START_DIVIDE_AMOUNT = 80d;

    public IntegralFunctional(double minIntervalIntegration, double maxIntervalIntegration) throws InvalidArgumentException {
        if (minIntervalIntegration > maxIntervalIntegration) {
            throw new InvalidArgumentException("Invalid interval value");
        }
        this.minIntervalIntegration = minIntervalIntegration;
        this.maxIntervalIntegration = maxIntervalIntegration;
    }

    public double calculate(T function) throws InvalidArgumentException {
        if (!(minIntervalIntegration >= function.getMinInterval() && maxIntervalIntegration <= function.getMaxInterval())) {
            throw new InvalidArgumentException();
        }
        double high = 0;
        double divides = START_DIVIDE_AMOUNT;
        double funcVal = 0;
        double result = 0;
        result = 0;
        high = (maxIntervalIntegration - minIntervalIntegration) / divides;
        for (int j = 0; j <= divides; j++) {
            funcVal = function.calculate(minIntervalIntegration + high * j);
            result += funcVal * high;
        }

        return result;
    }
}
