package ru.omsu.functional.impl;

import ru.omsu.functions.FunctionWithOneArgument;
import ru.omsu.functions.exception.InvalidArgumentException;
import ru.omsu.functional.Functional;

public class SumFunctional<T extends FunctionWithOneArgument> implements Functional<T> {
    public double calculate(T function) throws InvalidArgumentException {
        return function.calculate(function.getMinInterval())
                + function.calculate(function.getMaxInterval())
                + function.calculate((function.getMaxInterval() + function.getMinInterval())/2d);
    }
}
