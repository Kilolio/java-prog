package ru.omsu.functional;

import ru.omsu.functions.FunctionWithOneArgument;
import ru.omsu.functions.exception.InvalidArgumentException;

public interface Functional<T extends FunctionWithOneArgument> {
    double calculate(T function) throws InvalidArgumentException;
}
