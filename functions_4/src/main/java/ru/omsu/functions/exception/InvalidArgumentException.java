package ru.omsu.functions.exception;

public class InvalidArgumentException extends Exception {
    public InvalidArgumentException(String s) {
        super(s);
    }

    public InvalidArgumentException() {
    }
}
