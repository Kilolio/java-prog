package ru.omsu.functions;

import ru.omsu.functions.exception.InvalidArgumentException;

public interface FunctionWithOneArgument {
    double calculate(double arg) throws InvalidArgumentException;

    double getMinInterval();

    double getMaxInterval();
}
