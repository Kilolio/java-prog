package ru.omsu.functions.impl;

import ru.omsu.functions.FunctionWithOneArgument;
import ru.omsu.functions.exception.InvalidArgumentException;

public class TrigonometricFunction extends BaseFunction {

    public TrigonometricFunction(double a, double b, double minInterval, double maxInterval) throws InvalidArgumentException {
        super(a, b, minInterval, maxInterval);
    }

    public double calculate(double arg) throws InvalidArgumentException {
        if(!(arg >= minInterval && arg <= maxInterval)) {
            throw new InvalidArgumentException();
        }
        return a*Math.sin(b*arg);
    }
}
