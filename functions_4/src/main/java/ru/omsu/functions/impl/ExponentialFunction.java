package ru.omsu.functions.impl;

import ru.omsu.functions.exception.InvalidArgumentException;

public class ExponentialFunction extends BaseFunction {

    public ExponentialFunction(double a, double b, double minInterval, double maxInterval) throws InvalidArgumentException {
        super(a, b, minInterval, maxInterval);
    }

    public double calculate(double arg) throws InvalidArgumentException {
        if(!(arg >= minInterval && arg <= maxInterval)) {
            throw new InvalidArgumentException();
        }
        return a*Math.exp(arg) + b;
    }
}
