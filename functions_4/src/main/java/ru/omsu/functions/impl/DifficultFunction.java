package ru.omsu.functions.impl;

import ru.omsu.functions.exception.InvalidArgumentException;

public class DifficultFunction extends BaseFunction {
    private double c;
    private double d;

    public DifficultFunction(double a, double b, double minInterval, double maxInterval, double c, double d) throws InvalidArgumentException {
        super(a, b, minInterval, maxInterval);
        this.c = c;
        this.d = d;
    }

    public double calculate(double arg) throws InvalidArgumentException {
        if(c*arg + d == 0) {
            throw new InvalidArgumentException("Divide by zero");
        }
        if(!(arg >= minInterval && arg <= maxInterval)) {
            throw new InvalidArgumentException();
        }
        return (a*arg+b)/(c*arg+d);
    }
}
