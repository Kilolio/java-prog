package ru.omsu.functions.impl;

import ru.omsu.functions.FunctionWithOneArgument;
import ru.omsu.functions.exception.InvalidArgumentException;

public abstract class BaseFunction implements FunctionWithOneArgument {
    double a;
    double b;
    double minInterval;
    double maxInterval;

    public BaseFunction(double a, double b, double minInterval, double maxInterval) throws InvalidArgumentException {
        this.a = a;
        this.b = b;
        this.minInterval = minInterval;
        this.maxInterval = maxInterval;
        if(minInterval > maxInterval) {
            throw new InvalidArgumentException("Invalid interval value");
        }
    }

    public double getMinInterval() {
        return minInterval;
    }

    public double getMaxInterval() {
        return maxInterval;
    }
}
